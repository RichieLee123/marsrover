import RoverMap.Grid

case class RoverMap(map: Grid, roverLocation: (Int, Int), facing: Direction) {
  def step: RoverMap = {
    def produceNewMap(newLocation: (Int,Int)) = {
      val newMap = Array.ofDim[Int](map.length, map(0).length)
      newMap(newLocation._1)(newLocation._2) = 1
      RoverMap(newMap, newLocation, facing)
    }

    facing match {
      case LEFT =>
        if (roverLocation._1 == 0)
          produceNewMap(map(0).length - 1, roverLocation._2)
        else
          produceNewMap(roverLocation._1 - 1, roverLocation._2)
      case UP =>
        if (roverLocation._2 == 0)
          produceNewMap(roverLocation._1, map.length - 1)
        else
          produceNewMap(roverLocation._1, roverLocation._2 - 1)
      case DOWN =>
        if(roverLocation._2 == map.length - 1)
          produceNewMap(roverLocation._1, 0)
        else
          produceNewMap(roverLocation._1, roverLocation._2 + 1)
      case RIGHT =>
        if (roverLocation._1 == map(0).length - 1)
          produceNewMap(0, roverLocation._2)
        else
          produceNewMap(roverLocation._1 + 1, roverLocation._2)
    }
  }

  def turnLeft: RoverMap = RoverMap(map, roverLocation, facing.prev)
  def turnRight: RoverMap = RoverMap(map, roverLocation, facing.next)
}

object RoverMap {
  type Grid = Array[Array[Int]]

  def createMap(x: Int, y: Int, start: (Int, Int)): Either[InputValidationError, RoverMap] = {
    if (x < 2 || y < 2)
      return Left(MapTooSmallError)
    if (start._1 > x-1 || start._2 > y-1)
      return Left(StartOutOfBoundsError)

    val map = Array.ofDim[Int](x,y)
    map(start._1)(start._2) = 1 // mutation here isn't ideal but im sort of committed at this point

    Right(RoverMap(map, start, facing = UP))
  }
}

trait InputValidationError {
  val errorMessage: String
}
case object StartOutOfBoundsError extends InputValidationError {
  override val errorMessage: String = "The rover starting location was not within the bounds of the game board"
}
case object MapTooSmallError extends InputValidationError {
  override val errorMessage: String = "The map size cannot be smaller than 2 on either axis"
}