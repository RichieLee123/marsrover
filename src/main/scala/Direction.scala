sealed trait Direction {
  def next: Direction
  def prev: Direction
}

case object UP extends Direction {
  override def next: Direction = RIGHT
  override def prev: Direction = LEFT
}
case object LEFT extends Direction {
  override def next: Direction = UP
  override def prev: Direction = DOWN
}
case object DOWN extends Direction {
  override def next: Direction = LEFT
  override def prev: Direction = RIGHT
}
case object RIGHT extends Direction{
  override def next: Direction = DOWN
  override def prev: Direction = UP
}

