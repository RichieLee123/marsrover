import scala.io.StdIn.readLine
import scala.util.Try

object Main extends App {
  val stringToInt: String => Option[Int] = str => Try(str.toInt).toOption

  var roverMap = RoverMap.createMap(5,5,(2,2)).right.get
  println(s"state: $roverMap")

  Iterator.continually{
    println("Type an input (options: [step, turn left, turn right, exit])")
    readLine.toLowerCase()
  }
    .takeWhile(_ != "exit")
    .foreach {
      case "turn left" =>
        roverMap = roverMap.turnLeft
        println(s"state: $roverMap")
      case "turn right" =>
        roverMap = roverMap.turnRight
        println(s"state: $roverMap")
      case "step" =>
        roverMap = roverMap.step
        println(s"state: $roverMap")
      case _ =>
        println("input not recognised")
        println(s"state: $roverMap")
    }
}
