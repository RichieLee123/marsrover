import RoverMap.Grid
import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class RoverMapTests extends AnyFlatSpec with Matchers with EitherValues {
  behavior of "CreateMap"

  it should "create a new map with a valid size and start co-ordinates" in {
    val expectedFirstMap: Grid =
      Array(
        Array(1,0,0,0,0),
        Array(0,0,0,0,0),
        Array(0,0,0,0,0),
        Array(0,0,0,0,0),
        Array(0,0,0,0,0)
      )
    val firstOutcome = RoverMap.createMap(x = 5, y = 5, start = (0,0)).value
    firstOutcome.map shouldBe expectedFirstMap
    firstOutcome.facing shouldBe UP
    firstOutcome.roverLocation shouldBe (0,0)

    val expectedSecondMap: Grid =
      Array(
        Array(0,0,0,0,0),
        Array(0,0,0,0,0),
        Array(0,0,0,0,0),
        Array(0,0,0,0,0),
        Array(0,0,0,0,1)
      )
    val secondOutcome = RoverMap.createMap(x = 5, y = 5, start = (4,4)).value
    secondOutcome.map shouldBe expectedSecondMap
    secondOutcome.facing shouldBe UP
    secondOutcome.roverLocation shouldBe (4,4)

    val expectedThirdMap: Grid =
      Array(
        Array(0,0,0,0,0),
        Array(0,0,0,0,0),
        Array(0,0,1,0,0),
        Array(0,0,0,0,0),
        Array(0,0,0,0,0)
      )
    val thirdOutcome = RoverMap.createMap(x = 5, y = 5, start = (2,2)).value
    thirdOutcome.map shouldBe expectedThirdMap
    thirdOutcome.facing shouldBe UP
    thirdOutcome.roverLocation shouldBe (2,2)
  }

  it should "return an StartOutOfBoundsError when the rover starting location is out of bounds" in {
    RoverMap.createMap(x = 5, y = 5, start = (6,2)) shouldBe Left(StartOutOfBoundsError)
    RoverMap.createMap(x = 5, y = 5, start = (2,6)) shouldBe Left(StartOutOfBoundsError)
  }

  it should "return a MapSizeError when the map size is too small" in {
    RoverMap.createMap(x = 0, y = 5, start = (2,2)) shouldBe Left(MapTooSmallError)
    RoverMap.createMap(x = 1, y = 5, start = (2,2)) shouldBe Left(MapTooSmallError)
    RoverMap.createMap(x = 5, y = 0, start = (2,2)) shouldBe Left(MapTooSmallError)
    RoverMap.createMap(x = 5, y = 1, start = (2,2)) shouldBe Left(MapTooSmallError)
  }
}
