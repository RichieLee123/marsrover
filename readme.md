ITV tech test submission

to run:
 - `sbt run`
to run unit tests:
 - `sbt test`

notes:

Definitely not my finest work. i started out too ambitiously and wanted to take user input and validate it and then generate the board and all the rest of it.
by the time i created all of the initial unit tests and got them passing, i realize i didnt have enought time to get everything else done.

i really wouldve preferred to unit test around the movement mechanics and rollovers but time was definitely not on my side there. definitely a note to pick my battles when under the time crunch.

attached is also a document containing inital notes before i started work in intellij.